<?php

namespace Drupal\toolbar_plus\Event;

use Drupal\Component\EventDispatcher\Event;

class ShouldNotEditModeEvent extends Event {

  private $shouldNotEdit = FALSE;

  public function shouldNotEdit(): bool {
    return $this->shouldNotEdit;
  }

  public function setShouldNotEdit(): void {
    $this->shouldNotEdit = TRUE;
  }

}
