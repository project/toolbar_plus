<?php

declare(strict_types=1);

namespace Drupal\toolbar_plus\Attribute;

use Drupal\Component\Plugin\Attribute\AttributeBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The tool attribute.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class Tool extends AttributeBase {

  /**
   * Constructs a new Tool instance.
   *
   * @param string $id
   *   The name of the module.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $label
   *   The human-readable name of the module.
   * @param int $weight
   *   The weight or order the tool shows in the toolbar.
   */
  public function __construct(
    public readonly string $id,
    public readonly ?TranslatableMarkup $label,
    public readonly int $weight,
  ) {}

}
