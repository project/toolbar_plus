<?php

namespace Drupal\toolbar_plus\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\toolbar_plus\ToolPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Node Type Edit Form Alter
 *
 * Toolbar+ settings related to the content type.
 */
class NodeTypeEditFormAlter {

  public function __construct(
    protected ToolPluginManager $toolManager,
  ) {}

  use StringTranslationTrait;

  /**
   * Implements hook_form_FORM_ID_alter() for the 'field_config_edit' form ID.
   *
   * @param $form
   *   Forms.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *  The form state.
   */
  public function formAlter(&$form, FormStateInterface $form_state) {
    $node_type = $form_state->getFormObject()->getEntity();
    $options = [
      'last' => $this->t('The last tool used or Preview'),
    ];
    foreach ($this->toolManager->getDefinitions() as $tool => $definition) {
      $options[$tool] = $definition['label'];
    }

    $default_tool_default_value = NULL;
    if (\Drupal::moduleHandler()->moduleExists('edit_plus')) {
      $default_tool_default_value = $node_type->getThirdPartySetting('toolbar_plus', 'default_tool') ?? 'edit_plus';
    } elseif (!$default_tool_default_value) {
      $default_tool_default_value = $node_type->getThirdPartySetting('toolbar_plus', 'default_tool') ?? 'last';
    }

    $form['toolbar_plus'] = [
      '#type' => 'details',
      '#title' => $this->t('Toolbar+'),
      '#group' => 'additional_settings',
      'initial_mode' => [
        '#title' => $this->t('Initial mode'),
        '#type' => 'radios',
        '#options' => [
          'none' => $this->t('None'),
          // @todo Get edit mode from plugin manager once modes are pluggable.
          'edit_mode' => $this->t('Edit mode'),
        ],
        '#default_value' => $node_type->getThirdPartySetting('toolbar_plus', 'initial_mode') ?? 'none',
        '#description' => $this->t('Choose what mode will be activated when @type\'s are initially viewed.', ['@type' => $node_type->label()]),
      ],
      'default_tool' => [
        '#title' => $this->t('Default tool'),
        '#type' => 'radios',
        '#options' => $options,
        '#default_value' => $default_tool_default_value,
        '#description' => $this->t('Choose the tool that will be activated when @type\'s are initially edited. Subsequent edits of the same page will reactivate the last tool that was used.', ['@type' => $node_type->label()]),
        '#states' => [
          'visible' => [
            ':input[name="initial_mode"]' => ['value' => 'edit_mode'],
          ],
        ],
      ],
    ];

    array_unshift($form['actions']['submit']['#submit'], [$this, 'submit']);
  }

  /**
   * Submit handler.
   */
  public function submit(array &$form, FormStateInterface $form_state) {
    $node_type = $form_state->getFormObject()->getEntity();
    $node_type->setThirdPartySetting('toolbar_plus', 'default_tool', $form_state->getValue('default_tool'));
    $node_type->setThirdPartySetting('toolbar_plus', 'initial_mode', $form_state->getValue('initial_mode'));
  }

}
