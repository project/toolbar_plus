<?php declare(strict_types = 1);

namespace Drupal\toolbar_plus\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Toolbar + settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'toolbar_plus_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['toolbar_plus.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('toolbar_plus.settings');
    $form['toolbar_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Toolbar position'),
      '#options' => [
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
      '#default_value' => $config->get('toolbar_position') ?? 'left',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('toolbar_plus.settings')
      ->set('toolbar_position', $form_state->getValue('toolbar_position'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
