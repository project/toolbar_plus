<?php

declare(strict_types=1);

namespace Drupal\toolbar_plus;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for tool plugins.
 */
interface ToolInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Get mouse path.
   *
   * Returns the path to the mouse icon and the toolbar icon.
   *
   * @return array
   */
  public function getIconsPath(): array;

  /**
   * Build top bar.
   *
   * @return array
   *   A render array of items to include in the top bar when the tool is active.
   */
  public function buildTopBar(): array;

  /**
   * Build right sidebar.
   *
   * @return array
   *   A render array of items to include in the sidebar when the tool is active.
   */
  public function buildRightSideBar(): array;

  /**
   * Build left sidebar.
   *
   * @return array
   *   A render array of items to include in the sidebar when the tool is active.
   */
  public function buildLeftSideBar(): array;

  /**
   * (Optional) Get Sub Tools.
   *
   * Returns sub tool icons path and id.
   *
   * @return array
   */
  public function subTools(): array;

  /**
   * Add attachments.
   *
   * @param array $attachments
   *   The #attached array from $variables.
   *
   * @return void
   */
  public function addAttachments(array &$attachments): void;

  /**
   * Applies
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity this tool would apply to.
   *
   * @return bool
   *   Whether the tool plugin works on this type of entity.
   */
  public function applies(EntityInterface $entity): bool;

}
