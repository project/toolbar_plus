<?php declare(strict_types = 1);

namespace Drupal\toolbar_plus\EventSubscriber;

use Drupal\Core\Render\Markup;
use Drupal\toolbar_plus\ToolbarPlusUi;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\twig_events\Event\TwigRenderTemplateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class EntityUiWrapper implements EventSubscriberInterface {

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected ToolbarPlusUi $toolbarPlusUi,
  ) {}

  public function onTwigRenderTemplate(TwigRenderTemplateEvent $event): void {
    if ($this->toolbarPlusUi->shouldNotEditMode()) {
      return;
    }
    $variables = $event->getVariables();
    $entity_info = $this->getEntityInfo($variables);
    if (!empty($entity_info)) {
      // Give the rendered entity an AJAX wrapper so it can be updated as
      // changes are made.
      $output = $event->getOutput();

      // @todo Is it better if this loads the entity_view_display instead of the config directly?
      // Is this entity's display managed by layout builder?
      $config_name = sprintf('core.entity_view_display.%s.%s.%s',
        $entity_info['entity_type'],
        $entity_info['bundle'],
        $entity_info['view_mode'],
      );

      $config = $this->configFactory->get($config_name);
      // Check that this view mode isn't just falling back to default. If so, load
      // the default.
      if ($entity_info['view_mode'] !== 'default' && empty($config->getRawData())) {
        $config_name = sprintf('core.entity_view_display.%s.%s.%s',
          $entity_info['entity_type'],
          $entity_info['bundle'],
          'default',
        );
        $config = $this->configFactory->get($config_name);
      }

      $classes = 'toolbar-plus-entity-wrapper';
      if ($config->get('third_party_settings.layout_builder.enabled') === TRUE) {
        $classes .= ' layout-builder-entity-wrapper';
      }

      $wrapped_output = sprintf('<div class="%s" data-toolbar-plus-entity-wrapper="%s::%s::%s" data-toolbar-plus-view-mode="%s">%s</div>', $classes, $entity_info['entity_type'], $entity_info['entity_id'], $entity_info['bundle'], $entity_info['view_mode'], $output->__toString());
      $event->setOutput(Markup::create($wrapped_output));
    }
  }


  private function getEntityInfo(array $variables) {
    if (!empty($variables['elements']['#toolbar_plus_entity'])) {
      // Regular entities.
      return $variables['elements']['#toolbar_plus_entity'];
    } elseif (!empty($variables['elements']['content']['#toolbar_plus_entity'])) {
      // Inline blocks.
      return $variables['elements']['content']['#toolbar_plus_entity'];
    }
  }


  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      TwigRenderTemplateEvent::class => ['onTwigRenderTemplate'],
    ];
  }

}
