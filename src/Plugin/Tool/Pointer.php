<?php

declare(strict_types=1);

namespace Drupal\toolbar_plus\Plugin\Tool;

use Drupal\toolbar_plus\Attribute\Tool;
use Drupal\toolbar_plus\ToolPluginBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the tool.
 */
#[Tool(
  id: 'pointer',
  label: new TranslatableMarkup('Preview'),
  weight: 0,
)]
final class Pointer extends ToolPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getIconsPath(): array {
    $path = $this->extensionList->getPath('toolbar_plus');
    return [
      'toolbar_button_icons' => [
        'pointer' => "/$path/assets/cursor.svg",
        'refresh' => "/$path/assets/refresh.svg",
        'save' => "/$path/assets/save.svg",
        'discard-changes' => "/$path/assets/discard-changes.svg",
      ],
    ];
  }

}
