import * as toolPluginBase from '../../../js/toolbar_plus/tool-plugin-base.js';


class Pointer extends toolPluginBase.ToolPluginBase {
  id = 'pointer';
}


/**
 * Register the pointer plugin.
 */
Drupal.ToolbarPlus.PluginManager.registerPlugin(new Pointer());
