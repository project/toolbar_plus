<?php

namespace Drupal\toolbar_plus_entity_workflow\Routing;

use Drupal\toolbar_plus\ToolbarPlusUi;
use Drupal\Core\Routing\EnhancerInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Swaps the controller for routes that need to check for an active workspace.
 */
class BeforeEntityWorkflowEnhancer implements EnhancerInterface {

  public function __construct(
    protected readonly ToolbarPlusUi $toolbarPlusUi,
  ) {}

  /**
   * Enhance.
   *
   * Flag that reloading the page in order to get the editable page elements
   * requires a workspace.
   * @see WorkspaceSwitcher.
   */
  public function enhance(array $defaults, Request $request) {
    if (!\Drupal::currentUser()->hasPermission('use toolbar plus edit mode')) {
      return $defaults;
    }
    if ($this->toolbarPlusUi->getEditModeState() !== 'enabled') {
      return $defaults;
    }

    /** @var \Symfony\Component\Routing\Route $route */
    $route = $defaults[RouteObjectInterface::ROUTE_OBJECT];
    // Reload via AJAX controller.
    if ($defaults['_route'] === 'toolbar_plus.load_editable_page') {
      $route->setOption('_entity_workflow_content.require_workspace', TRUE);
      $route->setOption('_entity_workflow_content.entity_type_id', $defaults['entity_type']);
      $route->setOption('_toolbar_plus_entity_workflow.require_workspace', TRUE);
    }

    // Full page reload.
    if (str_starts_with($defaults['_route'], 'entity.') && str_ends_with($defaults['_route'], '.canonical' )) {
      $route->setOption('_entity_workflow_content.require_workspace', TRUE);
      [$_, $entity_type, $_] = explode('.', $defaults['_route']);
      $route->setOption('_entity_workflow_content.entity_type_id', $entity_type);
      $route->setOption('_toolbar_plus_entity_workflow.require_workspace', TRUE);
    }

    return $defaults;
  }

}
