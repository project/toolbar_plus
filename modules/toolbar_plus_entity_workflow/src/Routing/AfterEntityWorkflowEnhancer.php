<?php

namespace Drupal\toolbar_plus_entity_workflow\Routing;

use Drupal\toolbar_plus\ToolbarPlusUi;
use Drupal\Core\Routing\EnhancerInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\toolbar_plus_entity_workflow\Controller\WorkspaceSwitcher;

/**
 * Swaps the controller for routes that need to check for an active workspace.
 */
class AfterEntityWorkflowEnhancer implements EnhancerInterface {

  public function __construct(
    protected readonly ToolbarPlusUi $toolbarPlusUi,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function enhance(array $defaults, Request $request) {
    if ($this->toolbarPlusUi->getEditModeState() !== 'enabled') {
      return $defaults;
    }
    /** @var \Symfony\Component\Routing\Route $route */
    $route = $defaults[RouteObjectInterface::ROUTE_OBJECT];
    if ($route->hasOption('_toolbar_plus_entity_workflow.require_workspace')) {
      if ($defaults['_controller'] === '\Drupal\entity_workflow_content\Controller\WorkspaceSwitcherController::switcher') {
        // Use our own workspace switcher that will wrap the switcher form in
        // data-toolbar-plus-entity-wrapper and conditionally return it as an
        // AJAX response.
        if ($defaults['_route'] === 'toolbar_plus.load_editable_page') {
          $defaults['_controller'] = WorkspaceSwitcher::class . '::switcherAjax';
        } else {
          $defaults['_controller'] = WorkspaceSwitcher::class . '::switcherResponse';
          foreach ($route->getOption('parameters') as $name => $info) {
            if (!empty($info['type']) && str_contains($info['type'], 'entity:')) {
              $defaults['entity'] = $defaults[$name];
            }
          }
        }
      }
    }

    return $defaults;
  }

}
