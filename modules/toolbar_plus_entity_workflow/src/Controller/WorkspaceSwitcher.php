<?php
namespace Drupal\toolbar_plus_entity_workflow\Controller;

use Drupal\toolbar_plus\ToolbarPlusUi;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\toolbar_plus\LoadEditablePageResponseTrait;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\entity_workflow_content\Controller\WorkspaceSwitcherController;

/**
 * Workspace Switcher.
 *
 * Wraps the switcher form in data-toolbar-plus-entity-wrapper and conditionally
 * returns it as an AJAX response.
 */
class WorkspaceSwitcher extends WorkspaceSwitcherController {

  use LoadEditablePageResponseTrait;

  public function __construct(
    private readonly EntityDisplayRepositoryInterface $entityDisplayRepository,
    FormBuilderInterface $form_builder,
    private readonly ToolbarPlusUi $toolbarPlusUi,
  ) {
    $this->formBuilder = $form_builder;
  }

  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('entity_display.repository'),
      $container->get('form_builder'),
      $container->get('toolbar_plus.ui'),
    );
  }

  /**
   * Switcher Ajax.
   *
   * When a user enters edit mode they need to be in a workspace.
   * BeforeEntityWorkflowEnhancer and AfterEntityWorkflowEnhancer ties both
   * toolbar_plus.load_editable_page (ajax) and edit.node.canonical (full response)
   * routes into the workspace checker flow when edit mode is enabled.
   */
  public function switcherAjax(Request $request, EntityInterface $entity = NULL, string $view_mode = NULL) {
    $content = $this->getEmptyContent($entity, $view_mode);

    if (!$this->toolbarPlusUi->isValidViewMode($entity, $view_mode)) {
      $content['#markup'] = $this->t('Edit +: Invalid view mode for @label', ['@label' => $entity->label()]);
    }

    if (!$entity->access('edit')) {
      $content['#markup'] = $this->t("You don't have permission to edit @label.", ['@label' => $entity->label()]);
    } else {
      $content = parent::switcher($request);
      $content['#type'] = 'container';
      $content['#attributes']['data-toolbar-plus-entity-wrapper'] = $this->getWrapperId($entity, $view_mode);
    }

    return $this->getAjaxReplaceResponse($entity, $view_mode, $content);
  }

  /**
   * Switcher response.
   *
   * When a user enters edit mode they need to be in a workspace.
   * BeforeEntityWorkflowEnhancer and AfterEntityWorkflowEnhancer ties both the
   * toolbar_plus.load_editable_page (ajax) and edit.node.canonical (full response)
   * routes into the workspace checker flow when edit mode is enabled.
   */
  public function switcherResponse(Request $request, EntityInterface $entity) {
    $content = parent::switcher($request);
    $content['#type'] = 'container';
    // @todo It's not always okay to hard code default here...
    $content['#attributes']['data-toolbar-plus-entity-wrapper'] = $this->getWrapperId($entity, 'default');
    return $content;
  }

}
