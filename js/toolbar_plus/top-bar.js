import * as toolPluginBase from './tool-plugin-base.js';

(($, Drupal, once) => {

  /**
   * Refresh button.
   */
  Drupal.behaviors.ToolbarPlusRefresh = {
    attach: (context, settings) => {
      once('ToolbarPlusRefresh', '#toolbar-plus-refresh', context).forEach(refreshButton => {
        refreshButton.onclick = (e) => {
          location.reload();
        }
      });
    }
  };

  /**
   * Save button
   */
  Drupal.behaviors.ToolbarPlusSave = {
    attach: (context, settings) => {
      once('ToolbarPlusSave', '#toolbar-plus-save', context).forEach(saveButton => {

        saveButton.onclick = (e) => {
          // @todo Temporarily use the edit+ save controller.
          // @todo This creates a dependency on edit+ when in reality the
          // @todo tempstore for edit+ and LB+ should bubble up to some parent
          // @todo module.
          const entityWrapperId = document.querySelector('[data-toolbar-plus-entity-wrapper]').dataset.toolbarPlusEntityWrapper;
          const parts = entityWrapperId.split('::');

          window.location = Drupal.url('edit-plus/tempstore/save/' + parts[0] + '.' + parts[1]+ '?destination=' + window.location.pathname);
        }
      });
    }
  };

  /**
   * Discard changes button
   */
  Drupal.behaviors.ToolbarPlusDiscardChanges = {
    attach: (context, settings) => {
      once('ToolbarPlusDiscardChanges', '#toolbar-plus-discard-changes', context).forEach(discardChangesButton => {

        discardChangesButton.onclick = (e) => {
          // @todo Temporarily use the edit+ save controller.
          // @todo This creates a dependency on edit+ when in reality the
          // @todo tempstore for edit+ and LB+ should bubble up to some parent
          // @todo module.
          const entityWrapperId = document.querySelector('[data-toolbar-plus-entity-wrapper]').dataset.toolbarPlusEntityWrapper;
          const parts = entityWrapperId.split('::');
          toolPluginBase.ajax({
            url: Drupal.url('edit-plus/tempstore/delete-confirm/' + parts[0] + '.' + parts[1]+ '?destination=' + window.location.pathname),
            dialogType: 'modal',
            dialog: {
              title: Drupal.t('Discard changes?'),
              width: 'auto',
              height: 'auto',
              dialogClass: 'discard-changes-dialog',
            },
          }).execute();
        }
      });
    }
  };

})(jQuery, Drupal, once);
