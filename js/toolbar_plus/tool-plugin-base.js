/**
 * Tool plugin base.
 *
 * A base class for toolbar+ plugins.
 */
export class ToolPluginBase {

  /**
   * Initialized the plugin.
   */
  init() {}

  /**
   * Initial edit.
   *
   * Allows plugins to provide extra actions on the initial edit of a page.
   * Subsequent edit's of the same page will not call this.
   */
  initialEdit() {}

  /**
   * Enable the tool.
   *
   * @returns {Promise}
   */
  enable() {
    const topBar = document.querySelector('#' + this.id + '-top-bar');
    if (topBar) {
      topBar.classList.remove('toolbar-plus-hidden');
    }
    return Promise.resolve();
  }

  /**
   * Disable the tool.
   *
   * @param editModeDisabled
   *   False if we are just disabling the tool to switch to another tool.
   *   True if we are exiting edit mode.
   * @returns {Promise}
   */
  disable(editModeDisabled = false) {
    const topBar = document.querySelector('#' + this.id + '-top-bar');
    if (topBar) {
      topBar.classList.add('toolbar-plus-hidden');
    }
    return Promise.resolve();
  }

}

/**
 * Ajax.
 *
 * Wraps Drupal.ajax in order to pass along toolbar+ state in query parameters.
 *
 * @param settings
 *   Settings for Drupal.ajax.
 *
 * @returns {Drupal.Ajax}
 *   An instance of Drupal.ajax with query parameters appended to the URL.
 */
export const ajax = (settings) => {
  const url = new URL(window.location.origin + settings.url);
  url.searchParams.set('editMode', Drupal.ToolbarPlus.getCookieValue('editMode') ?? 'disabled');
  settings.url = url.toString();
  return Drupal.ajax(settings);
}
